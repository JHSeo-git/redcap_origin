/* main.js v1.0 | 2020.04.18 | UX/UI, HyunHyun Lee */

$(function () {
  // scroll top
  $(".scrolltop").click(function () {
    $(".wrap").animate({ scrollTop: 0 }, 400);
    return false;
  });
});

// hide/show scroll top
$(window).scroll(function () {
  if ($(this).scrollTop() > 200) {
    $(".scrolltop").fadeIn();
  } else {
    $(".scrolltop").fadeOut();
  }
});
