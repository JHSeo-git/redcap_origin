/* businessTravel.js v1.0 | 2020.08.13 | UX/UI, JHSeo */

// set bg img size
function setBgSize() {
  var contents = $("#contents");

  var bgImg = $(".section-template-list #btms-bg");
  bgImg.height(contents.height());
}

$(window).load(function () {
  setBgSize();
});

$(document).ready(function () {
  // obt
  function blinking(where, ms) {
    var steps = where.find(".move-box .steps li.target");

    $(".move-box .steps li.target").removeClass("on");
    steps.each(function (index) {
      setTimeout(function () {
        steps.eq(index).addClass("on");
      }, ms * (index + 1));
    });
  }

  blinking($(".contents__main.obt__main"), 500);
  blinking($(".contents__main.cti__main"), 500);

  // btms4.0
  function slideLinks1() {
    var $links = $("#btms-tab01 .flash-box ul li");
    $links.each(function (index) {
      var $this = $(this);
      $this.delay(index * 1500).queue(function () {
        // $links.removeClass('on');
        $this.addClass("on").dequeue();
      });
    });
    var setStop = setTimeout(slideLinks1, 25000);
    clearTimeout(setStop);
  }

  function slideLinks2() {
    var $links = $("#btms-tab02 .flash-box ul li");
    $links.each(function (index) {
      var $this = $(this);
      $this.delay(index * 1500).queue(function () {
        // $links.removeClass('on');
        $this.addClass("on").dequeue();
      });
    });
    var setStop2 = setTimeout(slideLinks2, 15000);
    clearTimeout(setStop2);
  }

  slideLinks1();
  $(".contents__main .tablist li").on("click", "a", function () {
    if ($(this).attr("rel") == "btms-tab02") {
      slideLinks2();
    } //else if($(this).attr('rel') == 'tech-tab01'){
    //slideLinks1();
    //}
  });

  // businessTravel
  function blinkingTab1(ms) {
    var $steps = $("#bt-tab01 .move-box .steps li.target");

    //$("#bt-tab01 .move-box .steps li.target").removeClass("on");
    $steps.each(function (index) {
      setTimeout(function () {
        $steps.eq(index).addClass("on");
      }, ms * (index + 1));
    });
  }
  function blinkingTab2(ms) {
    var $steps = $("#bt-tab02 .move-box .steps li.target");

    //$("#bt-tab02 .move-box .steps li.target").removeClass("on");
    $steps.each(function (index) {
      setTimeout(function () {
        $steps.eq(index).addClass("on");
      }, ms * (index + 1));
    });
  }
  function blinkingTab3(ms) {
    var $steps = $("#bt-tab03 .move-box .steps li.target");

    //$("#bt-tab03 .move-box .steps li.target").removeClass("on");
    $steps.each(function (index) {
      setTimeout(function () {
        $steps.eq(index).addClass("on");
      }, ms * (index + 1));
    });
  }
  function blinkingTab4(ms) {
    var $steps = $("#bt-tab04 .move-box .steps li.target");

    //$("#bt-tab04 .move-box .steps li.target").removeClass("on");
    $steps.each(function (index) {
      setTimeout(function () {
        $steps.eq(index).addClass("on");
      }, ms * (index + 1));
    });
  }

  blinkingTab1(500);
  $(".contents__main.bt__main .tablist li").on("click", "a", function () {
    if ($(this).attr("rel") == "bt-tab02") {
      blinkingTab2(500);
    } else if ($(this).attr("rel") == "bt-tab03") {
      blinkingTab3(500);
    } else if ($(this).attr("rel") == "bt-tab04") {
      blinkingTab4(500);
    }
  });

  // hotel management
  function slideLinkshm1() {
    var $links = $("#hm-flash-01 ul li");
    $links.each(function (index) {
      var $this = $(this);
      $this.delay(index * 500).queue(function () {
        // $links.removeClass('on');
        $this.addClass("on").dequeue();
      });
    });
    var setStop = setTimeout(slideLinkshm1, 25000);
    clearTimeout(setStop);
  }
  function blinkingTabHm1(ms) {
    var $steps = $("#hm-tab01 .move-box .steps li.target");

    //$("#bt-tab01 .move-box .steps li.target").removeClass("on");
    $steps.each(function (index) {
      if ($steps.length - 1 == index) {
        setTimeout(function () {
          slideLinkshm1();
        }, ms * (index + 2));
      }
      setTimeout(function () {
        $steps.eq(index).addClass("on");
      }, ms * (index + 1));
    });
  }

  function blinkingTabHm2(ms) {
    var $steps = $("#hm-tab02 .move-box .steps li.target");

    //$("#bt-tab01 .move-box .steps li.target").removeClass("on");
    $steps.each(function (index) {
      setTimeout(function () {
        $steps.eq(index).addClass("on");
      }, ms * (index + 1));
    });
  }

  blinkingTabHm1(500);
  $(".contents__main.bt__main .tablist li").on("click", "a", function () {
    if ($(this).attr("rel") == "hm-tab02") {
      blinkingTabHm2(500);
    }
  });

  // checkbox
  var checking = $(".contents__main .input__tab").find(
    "input[type='checkbox'] + label"
  );
  checking.each(function () {
    checking.on("click", function () {
      var checks = $(".contents__main .input__tab").find(
        "input[type='checkbox']"
      );
      if (checks.is(":checked")) {
        checks.prop("checked", true).parent().next(".accor-content").hide();
      } else {
        checks.prop("checked", false).parent().next(".accor-content").show();
      }
      setBgSize();
    });
  });
});
