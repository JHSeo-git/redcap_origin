﻿
/* popup */
function layerShow($thisI){
	var	$thisL = $(window).width()/2 - $('#'+$thisI).outerWidth(true)/2,
		$thisT =  $(window).height()/2 - $('#'+$thisI).outerHeight(true)/2;
	//$('body,html').css('overflow','hidden');  페이지 스크롤 되도록 수정해 놈
	$('body').append('<div class="dim" aria-hidden="true"></div>');
	$('#'+ $thisI).css({'top':$thisT,'left':$thisL}).fadeIn().attr('tabIndex',0).focus();

	$(document).on('click','.layer-close',function(){
		$('.dim').remove();
		$('html, body').removeAttr('style');
		$('.layer').fadeOut().removeAttr('style').removeAttr('tabIndex');
		var thisIt = "layerShow('" + $thisI + "')";

		$('[onclick="' + thisIt + '"]').focus();

	});

	$('.layer-swipe').resize(); //팝업 안에 slick swipe 사용 시 필요함/ .layer-swipe로 제어
};

$(document).ready(function() {
	var pub_ui = {};

	pub_ui.load=function(){
		//pub_ui.layer();
		pub_ui.init();
	};

	/* pub_ui.layer */
	pub_ui.layer=function(){
		var $layerControl = $('.layer-open');
		function layer(){
			$layerControl.on('click',function(){
				var $thisId =$(this).attr('rel')
				$(this).attr('tabIndex',-1);
				layerShow($thisId);
			});
			$(document).on('click','.layer-close',function(){
				$('.dim').remove();
				$('html, body').removeAttr('style');
				$('.layer').fadeOut().removeAttr('style').removeAttr('tabIndex');
				$layerControl.each(function(e){
					if($layerControl.attr('tabIndex') == -1){
						$(this).removeAttr('tabIndex').focus();
					};
				});
			});
		};
	}

	/* pub_ui.init */
	pub_ui.init=function() {
		// GNB
		function gnbArea(){
			var gnbList = $('#gnb > ul > li > a');
				depth2Link = $('#gnb li.gnb-dep2-item > a');
				depth3Link = $('#gnb li.gnb-dep3-item > a');
				depth4Button = $('#gnb li.gnb-dep3-item .btns-dep4');
				spd = 0;

			gnbList.on('mouseenter',function(){
				var $this = $(this);
				if ($(this).attr('aria-expanded') == 'false'){
					$this.attr('aria-expanded', 'true');
					$this.parents('li').siblings().find('> a').attr('aria-expanded', 'false');
					if ($(this).next('.gnb-category').length) {
						$this.next().css('visibility', 'visible');
						$this.parents('li').siblings().find('.gnb-category').css('visibility', 'hidden');
						if (!$('body').find('.gnb-dim').length) {
							$('.header.intro').addClass('active');
							if ($('#fullpage').length) {
								$('#fullpage').append('<div class="gnb-dim"/></div>');
							} else {
								$('body').append('<div class="gnb-dim"/></div>');
							}
						}
					} else {
						if ($('#fullpage').length) {
							$('#fullpage').find('.gnb-dim').remove();
						} else {
							$('body').find('.gnb-dim').remove();
						}
					}
				}

				//1뎁스 호버시
				$this.parent('li').bind('mouseleave', function() {
					$this.attr('aria-expanded', 'false');
					$this.next('.gnb-category').css('visibility', 'hidden');
					//$this.next().find('li.gnb-dep2-item:first-child > a').addClass('on');
				});
				//1뎁스 떠날때
				$this.parents('ul').bind('mouseleave', function() {
					setTimeout(function(){
						$('.header.intro').removeClass('active');
						$('body').find('.gnb-dim').remove();
					}, 0);
				});

				//뎁스초기화
				depth2Link.removeClass('on');
				depth3Link.removeClass('on');
				//$('.gnb-dep3').hide();
				//$this.next().find('li.gnb-dep2-item:first-child > a').addClass('on').next().show();
			});

			depth2Link.on('mouseenter',function(){
				var $this = $(this);
				if(!$(this).hasClass('on')){
					depth2Link.removeClass('on');
					depth3Link.removeClass('on');
					$(this).addClass('on');

					//depth2Link.next().hide();
					//depth3Link.next().hide();
					//$(this).next().show();
				}
			});

			depth3Link.on('mouseenter',function(){
				var $this = $(this);
				if(!$(this).hasClass('on')){
					depth3Link.removeClass('on');
					$(this).addClass('on');
					//depth3Link.next().hide();
					//$(this).next().show();
				}
			});
			depth4Button.on('click', function(){
				var $this = $(this);
				if(!$this.hasClass('on')){
					$this.attr('aria-expanded', 'true').text('닫힘');
					$this.next().show();
				}else {
					$this.attr('aria-expanded', 'false').text('열림');
					$this.next().hide();
				}
				$this.toggleClass('on');
			})
			//gnb 내 언어변경 부분
			var langArea = $('#gnb .multi-lang-area'),
				userLang = langArea.find('.user-lang');

			userLang.on('mouseenter', function(){
				$('.header.intro').addClass('active');
			});

			langArea.on('mouseleave', function(){
				$('.header.intro').removeClass('active');
				userLang.removeClass('on');
			});

			userLang.on('click', function(){
				if ($(this).hasClass('on')) {
					$(this).removeClass('on');
				} else {
					$(this).addClass('on');
				}
			});
		};gnbArea();

		//LOCATION
		var sub_location = {
			init:function(){
				this.btn = $('.location .location-menu strong');
				this.addEvent();
			},
			addEvent:function(){
				this.btn.parent().on('click', function(){
					

					//var $target = $(this).parent('.location-menu');
					var $target = $(this);
					//var $otherTarget = $(this).parents'li').siblings('li');
					var $otherTarget = $(this).parent('li').siblings('li');
					//var $deptharea = $(this).next('.dep-list');
					$deptharea = $(this).find('.dep-list');

          				var targetClass = $target.attr('class')||'';

          				if(targetClass === 'location-menu on'){
              					$deptharea.slideUp(250).parents('li').removeClass('on');
						$target.removeClass('on');
          				}else{
              					$target.addClass('on').find('.dep-list').slideDown(250);
          				}

          				$otherTarget.find('.location-menu').removeClass('on').find('.dep-list').slideUp(250);

					$deptharea.bind('mouseleave', function(){
						//$(this).slideUp(250).parents('li').removeClass('on');
						$(this).slideUp(250).parents('li').removeClass('on');
						$target.removeClass('on');
					});


				});
			}
		};sub_location.init();

		// DATEPICKER
		/*
		$('.ipt-calender input').datepicker({
			dayNamesMin:[ '일', '월', '화', '수', '목', '금', '토'],
			monthNames:['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
			dateFormat:'yy-mm-dd',
			showOtherMonths:true,
			selectOtherMonths:true,
			showOn: 'button',
			yearSuffix:'.',
			currentText: '오늘',
			showMonthAfterYear: true,
			buttonImage: '../../img/btn/btn_calender.png',
			buttonImageOnly: true,
			buttonText: '달력보기'
		});
		*/

		// PORTFOLIO
		function portfolio() {
			$('.portfolio-box .default .inner > a').on('click', function(){
				$(this).parents('li').addClass('on').siblings().addClass('off');
			});
			$('.shortcuts a').on('click', function(){
					$(this).parents('li').siblings().removeClass('on').addClass('off');
					$(this).parents('li').removeClass('off').addClass('on');
			});
		};portfolio();

		function portfolioSwipe() {
			$('.portfolio-swipe').slick({
				infinite: false,
			});
		};portfolioSwipe();

		// ACCORDION
		function accordion() {
			var $accorName ='.ani-accordion',
				$control = '.btn-accordion',
				$accorBox = '.accor-content';
			$($accorName).find($control).on('click',function() {
				var $this = $(this);
				var idx = $this.closest('li').index();
				function show(){
					hide();
					console.log($this.parents('div').hasClass('layer'))
					$this.addClass('on').closest('li');
					$this.attr('aria-expanded',true);
					$this.parents('li').find($accorBox).stop().slideDown(function() {
						if (!$this.parents('.layer').size() == 1) {
							var scrH = $(this).parents('li').eq(0).offset();
							$('html,body').animate({'scrollTop':scrH.top})
						}
					});
				};
				function hide() {
					$this.parents($accorName).find($control).removeClass('on');
					$this.attr('aria-expanded',false);
					$this.parents($accorName).find($accorBox).stop().slideUp();
				};
				$this.hasClass('on') ? hide() : show();
			});

			var termsList = '.terms-list input'
			$(termsList).on('click', function() {
				if($(this).is(':checked') && $(this).parents('li').find('.btn-accordion').hasClass('on') == false) {
					$(this).parents('li').find('.btn-accordion').attr('aria-expanded',true).trigger('click');
				}
			});
		};accordion();

		// 토글
		function toggle_ui(){
			$('.bt-toggle').on('click', function(){
				if (!$(this).hasClass('on')) {
					$(this).addClass('on');
					$(this).next('.hidden-cont').show();
				} else {
					$(this).removeClass('on');
					$(this).next('.hidden-cont').hide();
				}
			});
		};toggle_ui();

		// TAB
		function tabs(){
			var rows = [],
				boxId,
				$this=$('[class^=tab-ty]'),
				$child=$this.find('a');
			$child.on('click',function(){
				boxId = $(this).attr('rel');
				$(this).attr('aria-selected','true').closest('li').siblings('li').find('a').attr('aria-selected','false');
				$('#'+boxId).addClass('on').siblings('.tab-panel').removeClass('on');
			});
		};tabs();

		function categoryTabs(){
			var rows = [],
				boxId,
				$this=$('[class^=category-tab]'),
				$child=$this.find('a');
			$child.on('click',function(){
				boxId = $(this).attr('rel');
				$(this).attr('aria-selected','true').closest('li').siblings('li').find('a').attr('aria-selected','false');
				$('#'+boxId).addClass('on').siblings('.category-panel').removeClass('on');
			});
		};categoryTabs();

		function mapTabs(){
			var rows = [],
				boxId,
				$this=$('[class^=map-tab]'),
				$child=$this.find('a');
			$child.on('click',function(){
				boxId = $(this).attr('rel');
				$(this).attr('aria-selected','true').closest('li').siblings('li').find('a').attr('aria-selected','false');
				$('#'+boxId).addClass('on').siblings('li').removeClass('on');
			});
		};mapTabs();

		$('.branch-map-box .layer-map .close').on('click', function(){
			$(this).parents('.layer-map').hide();
		});

		// CHECKRADIO
		function checkRadio(){
			$('.select-group .btn-round').on('click',function(){
				if ($(this).attr('aria-selected') == 'false') {
					$(this).attr('aria-selected', 'true');
					$(this).parent().siblings().find('button').attr('aria-selected', 'false');
				}
			});
		};checkRadio();
	};

	$('#user_lang.add').on('mouseenter', function(){
        $('.header.intro').addClass('active');
    });

    $('.multi_lang_area.added').on('mouseleave', function(){
        $('.header.intro').removeClass('active');
        $('#user_lang').removeClass('on');
    });

    $('#user_lang.add').on('click', function(){
        if ($(this).hasClass('on')) {
            $(this).removeClass('on');
        } else {
            $(this).addClass('on');
        }
    });

    var checking = $(".terms-list.fix .accor-tab").find("input[type='checkbox'] + label");
	checking.each(function(){
		checking.on("click", function(){
			var checks =$(".terms-list.fix .accor-tab").find("input[type='checkbox']");
			if( checks.is(":checked") ){
				checks.prop("checked", true).parent().next(".accor-content").hide();
			}else{
				checks.prop("checked", false).parent().next(".accor-content").show();
			}
		});
	});

	pub_ui.load();
});

function pageControl() {
	//메뉴부분 호버에 대한 모션체크

	var $sec = $('.section'),
		thisSec = $('.section.active'),
		tmenu = $('.tech-menu .linker');

	tmenu.each(function(){
		$(this).on('click',function(){
			var menuNum = tmenu.index($(this));
			$sec.removeClass('active').hide();
			tmenu.removeClass('on');
			$(this).addClass('on');
			$sec.eq(menuNum).addClass('active').fadeIn();
			if($(".tech-menu .nav>ul>li:first-child>.dep2").find("li").eq(0)){
				//slideLinks1();
			}
			

			//Business Travel
			if(menuNum == 3){

				var belowMenus = $('.sec4 .tablist li');

				for(var i =0; i<belowMenus.length; i++){

					var isOn = $(belowMenus[i]).find('a').attr('aria-selected');

					if(isOn == 'true'){
						
						var onMenu = $(belowMenus[i]).find('a');
						$(onMenu).click();

					}

					
					
				}
				
						
			}


		});
	});

	// function slideLinks1() {
	// 	var $links = $('.tab-slide.tech-slide01 .tab-slide-cont');
	// 	$links.each(function(index) {
	// 		var $this = $(this);
	// 		$this.delay(index * 700).queue(function() {
	// 			$links.removeClass('on');
	// 			$this.addClass('on').dequeue();
	// 		});
	// 	});
	// 	setTimeout(slideLinks1, 7000);
	// }
	// function slideLinks2() {
	// 	var $links = $('.tab-slide.tech-slide02 .tab-slide-cont');
	// 	$links.each(function(index) {
	// 		var $this = $(this);
	// 		$this.delay(index * 800).queue(function() {
	// 			$links.removeClass('on');
	// 			$this.addClass('on').dequeue();
	// 		});
	// 	});
	// 	setTimeout(slideLinks2, 4800);
	// }
	//
	//2019.12.02 - ui.js
	function slideLinks1() {
		var $links = $('.tab-slide.tech-slide01 .tab-slide-cont');
		$links.each(function(index) {
			var $this = $(this);
			$this.delay(index * 1500).queue(function() {
				// $links.removeClass('on');
				$this.addClass('on').dequeue();
			});
		});
		var setStop = setTimeout(slideLinks1, 25000);
		clearTimeout(setStop);
	}

	function slideLinks2() {
		var $links = $('.tab-slide.tech-slide02 .tab-slide-cont');
		$links.each(function(index) {
			var $this = $(this);
			$this.delay(index * 1500).queue(function() {
				// $links.removeClass('on');
				$this.addClass('on').dequeue();
			});
		});
		var setStop2 = setTimeout(slideLinks2, 15000);
		clearTimeout(setStop2);
	}

	slideLinks1();
	$('.sec1 .tablist li').on('click','a',function(){
		if($(this).attr('rel') == 'tech-tab02') {
			slideLinks2();
		}//else if($(this).attr('rel') == 'tech-tab01'){
			//slideLinks1();
		//}
	})


	function blinking(btns,where,ms) {
		btns.on('click',function(){
			var steps = where.find('.move-box .steps li');

			$('.move-box .steps li').removeClass('on');
			steps.each(function(index){
				setTimeout(function(){
					steps.eq(index).addClass("on");
				},ms*(index+1));
			});
		});
	}

	blinking($('.sec4 .tablist li:last-child a'),$('.sec4 #tech-tab04_4'),350)
	blinking($('.tech-menu nav > ul > li:nth-child(2) .dep2 li:last-child a'),$('.sec5 #tech-tab05_1'),500)
	blinking($('.sec5 .tablist li:first-child a'),$('.sec5 #tech-tab05_1'),300)
	blinking($('.tech-menu nav > ul > li:nth-child(2) .dep2 li:first-child a'),$('.sec4 #tech-tab04_1'),250)
	blinking($('.sec4 .tablist li:first-child a'),$('.sec4 #tech-tab04_1'),250)
	blinking($('.tech-menu nav > ul > li:first-child .dep2 li:nth-child(2) a'),$('.sec2'),400)
	blinking($('.tech-menu nav > ul > li:first-child .dep2 li:last-child a'),$('.sec3'),350)
	blinking($('.sec4 .tablist li:nth-child(2) a'),$('.sec4 #tech-tab04_2'),350)
	blinking($('.sec4 .tablist li:nth-child(3) a'),$('.sec4 #tech-tab04_3'),500)


	$('.belong-maps').on('click','button',function(){
		$('.belong-maps button').removeClass('on');
		$(this).addClass('on');
	});

	
}