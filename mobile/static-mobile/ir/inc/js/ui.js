﻿	
/* popup */
function layerShow($thisI){

	var	$thisL = $(window).width()/2 - $('#'+$thisI).outerWidth(true)/2,
		$thisT =  $(window).height()/2 - $('#'+$thisI).outerHeight(true)/2;
	$('#'+ $thisI).css({'top':$thisT,'left':$thisL}).fadeIn().attr('tabIndex',0).focus();


	//$('#'+ $thisI).fadeIn().attr('tabIndex',0).focus();
	$(window).resize(function(){
		var thisLayer = $('#'+ $thisI);
		thisLayer.each(function(){
			thisLayer.find('.layer-inner').removeAttr('style');
			if ($(this).find('.flexible').length){
				var winHeight = $(window).height();
				var checkHeight = $(this).find('.layer-inner').height();
				if (checkHeight >= winHeight){
					$(this).find('.layer-inner').css('height', winHeight - 30);
				}
			}
		});
	}).resize();
};
function layerClose($this){
	var layerContent = $this.closest('.layer-cont').next();
	layerContent.trigger('click');
}

var pub_ui = {};

$(document).ready(function(){
	pub_ui.load=function(){
		pub_ui.layer();
		pub_ui.init();
	};

	/* pub_ui.layer */
	pub_ui.layer=function(){
		var $layerControl = $('.layer-open');
		function layer(){
			$layerControl.on('click',function(){
				var $thisId =$(this).attr('rel')
				$(this).attr('tabIndex',-1);
				layerShow($thisId);
			});
			$(document).on('click','.layer-close',function(){
				$('.dim').remove();
				$('html, body').removeAttr('style');
				$('.layer').fadeOut().removeAttr('style').removeAttr('tabIndex');
				$layerControl.each(function(e){
					if($layerControl.attr('tabIndex') == -1){
						$(this).removeAttr('tabIndex').focus();
					};
				});
			});
		};layer();
	}

	/* pub_ui.init */
	pub_ui.init=function(){
		// 로케이션
		function locationBar() {
			$('.location button').on('click', function() {
				if ($(this).attr('aria-expanded') == 'false') {
					$(this).next().slideDown(300);
					$(this).attr('aria-expanded', 'true');
				} else {
					$(this).next().slideUp(300);
					$(this).attr('aria-expanded', 'false');
				}
			});
			$('.location li a').each(function(){
				if ($(this).next('.nav-2dep').size() == 1 || $(this).next('.nav-3dep').size() == 1) {
					$(this).addClass('depth');
					$(this).on('click', function(){
						if(!$(this).hasClass('on')) {
							$(this).addClass('on').next().slideDown(300);
							$(this).parent().siblings().find('> a').removeClass('on');
							$(this).parent().siblings().find('.nav-3dep').slideUp(300);
						} else {
							$(this).removeClass('on').next().slideUp(300);
						}
					});
				}
			});
		};locationBar();

		// 전체메뉴
		function slideMenuBar() {
			slideMenuHeight();

			$('.header .btn-menu').on('click', function() { //open
				if(!$('.slidemenu-wrap').hasClass('on')) {
					$('.slidemenu-wrap').addClass('on');

					slideMenuHeight();
					stopBodyScrolling(true);
					$('.wrap .wrap-inr').attr('aria-hidden', 'true');
				}
			});
			$('.slidemenu-wrap .btn-close').on('click', function() { //close
				$('.slidemenu-wrap').removeClass('on');
				stopBodyScrolling(false);
				$('.wrap .wrap-inr').attr('aria-hidden', 'false');
			});

			$('.slidemenu-wrap .slidemenu-acco button').on('click', function(){
				if ($(this).attr('aria-expanded') == 'false') {
					$(this).next().slideDown(200);
					$(this).attr('aria-expanded', 'true');
				} else {
					$(this).next().slideUp(200);
					$(this).attr('aria-expanded', 'false');
				}
			});

			$('.slidemenu-navi li a').each(function(){
				if ($(this).next('.nav-2dep').size() == 1 || $(this).next('.nav-3dep').size() == 1) {
					$(this).addClass('depth');
					$(this).on('click', function(){
						if(!$(this).hasClass('on')) {
							$(this).addClass('on').next().slideDown(300);
							$(this).parent().siblings().find('> a').removeClass('on');
							$(this).parent().siblings().find('.nav-2dep').slideUp(300);
						} else {
							$(this).removeClass('on').next().slideUp(300);
						}
					});
				}
			});

			function slideMenuHeight() {
				var win = $(window).height();
				var top = $('.slidemenu-top').height();
				var height = (win-top);
				$('.slidemenu .scroll').css('height', height);
			}
		};slideMenuBar();

		stopBodyScrolling = function(checking) {
			if (checking) {
				//disable scroll
				$(window).on('scroll', function(event) {
					var x = window.scrollX,
						y = window.scrollY;
					window.scrollTo(x,y)
				});
				bodyScroll_Top = $(window).scrollTop();
				$('body').css({'overflow': 'hidden', 'position': 'fixed', 'left': 0, 'right': 0});
			} else {
				//enable scroll
				$(window).off('scroll');
				$(window).scrollTop(bodyScroll_Top);
				$('body').css({'overflow': '', 'position': '', 'left': '', 'right': ''});
			}
		}

		// QUICK
		function quickBtn(){
			var goTopBtn = $('.page-top');
			$(window).on('scroll', function(){
				var sTop = $(this).scrollTop();
				if( sTop > 500 ) goTopBtn.fadeIn();
				else goTopBtn.fadeOut();
			});
			goTopBtn.off('click').on('click' , function() {
				$('html, body').animate({
					scrollTop : 0
				}, 300);
				return false;
			});
		};quickBtn();

		/* 스와이프탭 */
		function swiperTab() {
			if ($('.tab-swiper-container').size() == 1) {
				var swiper = new Swiper('.tab-swiper-container', {
					slidesPerView: 3,
					slideToClickedSlide: true,
					freeMode: true,
				});
			}
		};swiperTab();

		// 토글
		function toggle_ui(){
			$('.bt-toggle').on('click', function(){
				if (!$(this).hasClass('on')) {
					$(this).addClass('on');
					$(this).next('.hidden-cont').show();
				} else {
					$(this).removeClass('on');
					$(this).next('.hidden-cont').hide();
				}
			});
		};toggle_ui();


		/* 탭 */
		function tabs(){
			var rows = [],
				boxId,
				$this=$('[class^=tab-ty]'),
				$child=$this.find('a');
			$child.on('click',function(){
				boxId = $(this).attr('rel');
				$(this).attr('aria-selected','true').closest('li').siblings('li').find('a').attr('aria-selected','false');
				$('#'+boxId).addClass('on').siblings('.tab-panel').removeClass('on');
			});
		};tabs();

		function mapTabs(){
			var rows = [],
				boxId,
				$this=$('[class^=map-tab]'),
				$child=$this.find('a');
			$child.on('click',function(){
				boxId = $(this).attr('rel');
				$(this).attr('aria-selected','true').closest('li').siblings('li').find('a').attr('aria-selected','false');
				$('#'+boxId).addClass('on').siblings('li').removeClass('on');
			});
		};mapTabs();

		/* tooltip */
		function tooltip(){
			$('.tooltip-a').on('click',function(){
				$(this).next('.tooltip-box').toggle();
				setTimeout(function(){$('.tooltip-box').hide()},5000);
			});
		};tooltip();

		function tooltip(){
			$('.tooltip-a').on('click',function(){
				$(this).next('.tooltip-box').toggle();
				setTimeout(function(){$('.tooltip-box').hide()},5000);
			});
		};tooltip();

		//tooltip-content show/hide type
		function tooltipCont(){
			$('.tooltip.only-btn').each(function () {
				$(this).on('click', function () {
					$(this).toggleClass('on');
					var tooltipDetail = $(this).closest('.tooltip-detail-wrap').find('.tooltip-detail');
					$(tooltipDetail).slideToggle();
				});
			});
		};tooltipCont();

		/* accordion */
		var rows = [];
		function accordion(){
			var $accorName ='.ani-accordion',
				$control = '.btn-accordion',
				$accorBox = '.accor-content';
			var headerHeight = $('.header-inr').height();
			$($accorName).find($control).on('click',function(){
				var $this = $(this);
				var idx = $this.closest('li').index();
				function show(){
					hide();
					console.log($this.parents('div').hasClass('layer'))
					$this.addClass('on').closest('li');
					$this.attr('aria-expanded',true);
					$this.parents('li').find($accorBox).stop().slideDown(function(){
						if (!$this.parents('.layer').size() == 1) {
							var scrH = $(this).parents('li').eq(0).offset();
							$('html,body').animate({'scrollTop':scrH.top - headerHeight})
						}
					});
				};
				function hide(){
					$this.parents($accorName).find('li').find($control).removeClass('on');
					$this.attr('aria-expanded',false);
					$this.parents($accorName).find($accorBox).stop().slideUp();
				};
				$this.hasClass('on') ? hide() : show();
			});

			var termsList = '.terms-list input'
			$(termsList).on('click', function() {
				if($(this).is(':checked') && $(this).parents('li').find('.btn-accordion').hasClass('on') == false) {
					$(this).parents('li').find('.btn-accordion').attr('aria-expanded',true).trigger('click');
				}
			});
		};accordion();

		/* depth menu */
		function depthMenuList() {
			$('.visual-area .dep-menu .menu').on('click', function(){
				if ($(this).attr('aria-expanded') == 'false') {
					$(this).attr('aria-expanded', 'true').next().slideDown(200);
				} else {
					$(this).attr('aria-expanded', 'false').next().slideUp(200);
				}
			});
		};depthMenuList();

		function selectList() {
			$('.select-list-area select').on('change', function() {
				var optionVal = $(this).find('> option:selected').val();
				var target = $(this).closest('.select-list-area').find('.select-list');
				target.each(function(){
					if (optionVal == $(this).attr('id')) {
						$(this).show();
					} else {
						$(this).hide();
					}
				});
			});
		};selectList();
	};

	pub_ui.load();
});
