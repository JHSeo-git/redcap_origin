/* mice.js v1.1 | 2020.08.18 | UX/UI, JH Seo */

// slick + popup override
function slickAdjust($thisI) {
  $(".layer-swipe").slick({
    slide: "div", //슬라이드 되어야 할 태그 ex) div, li
    infinite: true, //무한 반복 옵션
    slidesToShow: 1, // 한 화면에 보여질 컨텐츠 개수
    slidesToScroll: 1, //스크롤 한번에 움직일 컨텐츠 개수
    speed: 500, // 다음 버튼 누르고 다음 화면 뜨는데까지 걸리는 시간(ms)
    arrows: true, // 옆으로 이동하는 화살표 표시 여부
    dots: true, // 스크롤바 아래 점으로 페이지네이션 여부
    autoplay: false, // 자동 스크롤 사용 여부
    autoplaySpeed: 30000, // 자동 스크롤 시 다음으로 넘어가는데 걸리는 시간 (ms)
    pauseOnHover: true, // 슬라이드 이동	시 마우스 호버하면 슬라이더 멈추게 설정
    vertical: false, // 세로 방향 슬라이드 옵션
    prevArrow:
      "<button type='button' aria-label='Prev' class='slick-prev slick-arrow'>Previous</button>", // 이전 화살표 모양 설정
    nextArrow:
      "<button type='button' aria-label='Next' class='slick-next slick-arrow'>Next</button>", // 다음 화살표 모양 설정
    dotsClass: "slick-dots", //아래 나오는 페이지네이션(점) css class 지정
    draggable: true, //드래그 가능 여부
  });
  $(".layer-swipe").resize(); //팝업 안에 slick swipe 사용 시 필요함/ .layer-swipe로 제어

  var buttonTop =
    31 + $($(`#${$thisI} .layer-swipe .layer-img`)).height() / 2 + 34;
  $(".layer-swipe .slick-arrow").css({ top: `${buttonTop}px` });
}

function layerShow($thisI) {
  var $thisL = $(window).width() / 2 - $("#" + $thisI).outerWidth(true) / 2,
    $thisT = $(window).height() / 2 - $("#" + $thisI).outerHeight(true) / 2;
  $("#" + $thisI)
    .css({ top: $thisT, left: $thisL })
    .fadeIn()
    .attr("tabIndex", 0)
    .focus();

  //$('#'+ $thisI).fadeIn().attr('tabIndex',0).focus();
  $(window)
    .resize(function () {
      var thisLayer = $("#" + $thisI);
      thisLayer.each(function () {
        thisLayer.find(".layer-inner").removeAttr("style");
        if ($(this).find(".flexible").length) {
          var winHeight = $(window).height();
          var checkHeight = $(this).find(".layer-inner").height();
          if (checkHeight >= winHeight) {
            $(this)
              .find(".layer-inner")
              .css("height", winHeight - 30);
          }
        }
      });
    })
    .resize();

  slickAdjust($thisI);
}
function layerClose($this) {
  var layerContent = $this.closest(".layer-cont").next();
  layerContent.trigger("click");
}

$(document).ready(function () {
  var isVisible = false;
  $(".wrap").on("scroll", function () {
    if (checkVisible($(".mice-comp")) && !isVisible) {
      //$('.mice-comp').addClass('active');
      countStart();
      isVisible = true;
    }
  });

  function checkVisible(elem, eval) {
    if ($(elem) && $(elem).offset()) {
      eval = eval || "object visible";
      var docViewTop = $(window).scrollTop();
      var docViewBottom = docViewTop + $(window).height();

      var elemTop = $(elem).offset().top;
      var elemBottom = elemTop + $(elem).height();

      return elemBottom <= docViewBottom && elemTop >= docViewTop;
    }
  }

  function countStart() {
    $(".count-num").each(function () {
      var $this = $(this);
      $({ Counter: 0 }).animate(
        { Counter: $this.attr("data-stop") },
        {
          duration: 1000,
          easing: "swing",
          step: function (now) {
            $this.text(commaSeparateNumber(Math.ceil(now)));
          },
        }
      );
    });
  }

  function commaSeparateNumber(val) {
    while (/(\d+)(\d{3})/.test(val.toString())) {
      val = val.toString().replace(/(\d+)(\d{3})/, "$1" + "," + "$2");
    }
    return val;
  }

  function layer() {
    $(document).on("click", ".layer-close", function () {
      $(".layer-swipe").slick("unslick");
    });
  }
  layer();
});
