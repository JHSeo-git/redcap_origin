/* main.js v1.0 | 2020.04.17 | UX/UI, HyunHyun Lee */

$(function() {
    // quick link smooth scroll
    $('.navbar a').click(function() {
        event.preventDefault();
        $('.navbar li').removeClass('active');
        $(this).parent('li').addClass('active');
        $('html, body').animate({scrollTop: $($(this.hash)).offset().top }, 500);
    });
    // scroll top
    $('.scrolltop').click(function() {
        $('html, body').animate({scrollTop : 0}, 400);
	    return false;
    });
});

// hide/show scroll top 
$(window).scroll(function() {
	if ($(this).scrollTop() > 200) {
		$('.scrolltop').fadeIn();
	} else {
		$('.scrolltop').fadeOut();
	}
});